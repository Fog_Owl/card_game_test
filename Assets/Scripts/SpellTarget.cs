﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpellTarget : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        if (!GameManagerScript.Instance.IsPlayerTurn)
            return;

        CardController spell = eventData.pointerDrag.GetComponent<CardController>(),
                       target = GetComponent<CardController>();

        if (spell &&
            spell.Card.IsSpell &&
            spell.IsPlayerCard &&
            target.Card.IsPlaced &&
            GameManagerScript.Instance.CurrentGame.Player.Mana >= spell.Card.ManaCost)
        {
            var spellCard = (SpellCard)spell.Card;

            if((spellCard.SpellTarget == SpellCard.TargetType.ALL_CARD_TARGET && 
                target.IsPlayerCard) || 
                (spellCard.SpellTarget == SpellCard.TargetType.ENEMY_CARD_TARGET &&
                !target.IsPlayerCard))
            {
                GameManagerScript.Instance.ReduceMana(true, spell.Card.ManaCost);
                spell.UseSpell(target);
                GameManagerScript.Instance.CheckCardsForManaAvaliability();
            }

        }
    }
}
