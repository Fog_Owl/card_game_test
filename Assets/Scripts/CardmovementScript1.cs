﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Vector3 = UnityEngine.Vector3;
using DG.Tweening;
using UnityEngine.UI;

public class CardmovementScript1 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public CardController CC;


    Camera MainCamera;
    Vector3 offset; // хранит значение отступа центра от места карты, по которому произведён клик миши
    public Transform DefaultParant, DefaultTempCardParent;
    GameObject TempCardGO;
    public bool isDraggable; // Можно ли перетаскивать карту или нет

    int startID;
    
    private void Awake()
    {
        MainCamera = Camera.allCameras[0];
        TempCardGO = GameObject.Find("TempCardGo");
        GameManagerScript.Instance = FindObjectOfType<GameManagerScript>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        offset = transform.position - MainCamera.ScreenToWorldPoint(eventData.position);
        DefaultParant = DefaultTempCardParent = transform.parent;


        isDraggable = GameManagerScript.Instance.IsPlayerTurn &&
            (
            (DefaultParant.GetComponent<DropPlayScript>().Type == FieldType.SELF_HAND &&
            GameManagerScript.Instance.CurrentGame.Player.Mana >= CC.Card.ManaCost) ||
            (DefaultParant.GetComponent<DropPlayScript>().Type == FieldType.SELF_FIELD &&
           CC.Card.CanAttack)
            );
       

        if (!isDraggable)

            return;

        startID = transform.GetSiblingIndex();

        if(CC.Card.IsSpell || CC.Card.CanAttack)
            GameManagerScript.Instance.HighlightTargets(CC, true);




        TempCardGO.transform.SetParent(DefaultParant);
        TempCardGO.transform.SetSiblingIndex(transform.GetSiblingIndex());

        transform.SetParent(DefaultParant.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!isDraggable)

            return;


        Vector3 newPos = MainCamera.ScreenToWorldPoint(eventData.position); // Текущая позиция карты

        transform.position = newPos + offset;

        if (!CC.Card.IsSpell)
        {

            if (TempCardGO.transform.parent != DefaultTempCardParent)
                TempCardGO.transform.SetParent(DefaultTempCardParent);


            if (DefaultParant.GetComponent<DropPlayScript>().Type != FieldType.SELF_FIELD)
                CheckPos();
        }
        

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isDraggable)

            return;

        GameManagerScript.Instance.HighlightTargets(CC, false);

        transform.SetParent(DefaultParant);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        transform.SetSiblingIndex(TempCardGO.transform.GetSiblingIndex());
        TempCardGO.transform.SetParent(GameObject.Find("Canvas").transform);
        TempCardGO.transform.localPosition = new Vector3(2340, 0);
    }

    void CheckPos()
    {
        int newIndex = DefaultTempCardParent.childCount;

        for (int i = 0; i < DefaultTempCardParent.childCount; i++)
        {
            if (transform.position.x < DefaultTempCardParent.GetChild(i).position.x)
            {
                newIndex = i;

                if (TempCardGO.transform.GetSiblingIndex() < newIndex)
                    newIndex--;

                break;
            }
        }

        if (TempCardGO.transform.parent == DefaultParant)
            newIndex = startID;

        TempCardGO.transform.SetSiblingIndex(newIndex);
    }

    public void MoveToField(Transform field)
    {
        transform.SetParent(GameObject.Find("Canvas").transform);
        transform.DOMove(field.position, .5f);
    }
    public void MoveToTarget(Transform target)
    {
        StartCoroutine(MoveToTargetCor(target));
    }
    IEnumerator MoveToTargetCor(Transform target)
    {
        Vector3 pos = transform.position;
        Transform parent = transform.parent;
        int index = transform.GetSiblingIndex();


        if(transform.parent.GetComponent<HorizontalLayoutGroup>())
            transform.parent.GetComponent<HorizontalLayoutGroup>().enabled = false;


        transform.SetParent(GameObject.Find("Canvas").transform);

        transform.DOMove(target.position, .25f);

        yield return new WaitForSeconds(.25f);

        transform.DOMove(pos, .25f);

        yield return new WaitForSeconds(.25f);

        transform.SetParent(parent);
        transform.SetSiblingIndex(index);

        if(transform.parent.GetComponent<HorizontalLayoutGroup>())
            transform.parent.GetComponent<HorizontalLayoutGroup>().enabled = true;

    }
}

