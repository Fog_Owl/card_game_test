﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;



public class Card
{
    public enum AbilityType
    {
        NO_ABILITY,
        INSTANT_ACTIVE,
        DOUBLE_ATTACK,
        SHIELD,
        PROVOCATION,
        REGENERATOM_EACH_TURN,
        COUNTER_ATTACK
    }



    


    public string Name;
    public Sprite Logo;
    public int Attack, Defence, ManaCost;
    public bool CanAttack;

    public bool IsPlaced;

    public bool IsSpell;

    public List<AbilityType> Abilities;

    public bool HasAbility
    {
        get
        {
            return Abilities.Count > 0;
        }
    }

    public bool IsProvocation
    {
        get
        {
            return Abilities.Exists(x => x == AbilityType.PROVOCATION);
        }
    }

    
    public int TimesDealDamage;

    public bool IsAlive
    {
        get
        {
            return Defence > 0;
        }
    }

    public Card(string name, string logoPath, int attack, int defence, int manacost, AbilityType abilityType = 0)
    {
        Name = name;
        Logo = Resources.Load<Sprite>(logoPath);
        Attack = attack;
        Defence = defence;
        ManaCost = manacost;
        CanAttack = false;
        IsPlaced = false;

        Abilities = new List<AbilityType>();

        if (abilityType != 0)
            Abilities.Add(abilityType);

        

        TimesDealDamage = 0;
    }
    
    public Card(Card card)
    {
        Name = card.Name;
        Logo = card.Logo;
        Attack = card.Attack;
        Defence = card.Defence;
        ManaCost = card.ManaCost;
        CanAttack = false;
        IsPlaced = false;

        Abilities = new List<AbilityType>(card.Abilities);

        TimesDealDamage = 0;
    }
    
    public void GetDamage(int dmg)
    {
        if(dmg > 0)
        {
            if (Abilities.Exists(x => x == AbilityType.SHIELD))
                Abilities.Remove(AbilityType.SHIELD);
            else
                Defence -= dmg;

        }

        
    }

    public Card GetCopy()
    {
        
        return new Card(this);
    }
}



public class SpellCard : Card
{
    public enum SpellType
    {
        NO_SPELL,
        HEAL_ALL_FIELD_CARDS,
        DAMAGE_ENEMY_FIELD_CARDS,
        HEAL_ALL_HERO,
        DAMAGE_ENEMY_HERO,
        HEAL_ALL_CARD,
        DAMAGE_ENEMY_CARD,
        SHIELD_ON_ALL_CARD,
        PROVOCATION_ON_ALL_CARD,
        BUFF_CARD_DAMAGE,
        DEBUFF_CARD_DAMAGE
    }

    public enum TargetType
    {
        NO_TARGET,
        ALL_CARD_TARGET,
        ENEMY_CARD_TARGET
    }

    public SpellType Spell;
    public TargetType SpellTarget;
    public int SpellValue;

    public SpellCard(string name, string logoPath, int manacost, SpellType spellType = 0,
                     int spellValue = 0, TargetType  targetType = 0) : base(name, logoPath, 0, 0, manacost)
    {
        IsSpell = true;

        Spell = spellType;
        SpellTarget = targetType;
        SpellValue = spellValue;
    }

    public SpellCard(SpellCard card) : base (card)
    {
        IsSpell = true;

        Spell = card.Spell;
        SpellTarget = card.SpellTarget;
        SpellValue = card.SpellValue;
    }

    public new SpellCard GetCopy()
    {
        return new SpellCard(this);
    }
}

public static class CardManagerAll
{
    public static List<Card> allcards = new List<Card>();
}

public class CardManager : MonoBehaviour
{
    public void Awake()
    {
        CardManagerAll.allcards.Add(new Card("abadon","Sprites/abadon",5,5, 4));
        CardManagerAll.allcards.Add(new Card("alchemist", "Sprites/alchemist", 1, 3, 1));
        CardManagerAll.allcards.Add(new Card("ancient-apparition", "Sprites/ancient-apparition", 2, 3, 2));
        CardManagerAll.allcards.Add(new Card("anti-mage", "Sprites/anti-mage", 1, 8, 1));
        CardManagerAll.allcards.Add(new Card("arc-warden", "Sprites/arc-warden", 2, 2, 2));
        CardManagerAll.allcards.Add(new Card("axe", "Sprites/axe", 1, 1, 1));

        CardManagerAll.allcards.Add(new Card("provocation", "Sprites/provocation", 1, 2, 3, Card.AbilityType.PROVOCATION));
        CardManagerAll.allcards.Add(new Card("regeneration", "Sprites/regen", 4, 2, 5, Card.AbilityType.REGENERATOM_EACH_TURN));
        CardManagerAll.allcards.Add(new Card("doubleAttack", "Sprites/doubleAttack", 3, 2, 4, Card.AbilityType.DOUBLE_ATTACK));
        CardManagerAll.allcards.Add(new Card("instantActive", "Sprites/instant", 2, 1, 2,Card.AbilityType.INSTANT_ACTIVE));
        CardManagerAll.allcards.Add(new Card("shield", "Sprites/shield", 5, 1, 7, Card.AbilityType.SHIELD));
        CardManagerAll.allcards.Add(new Card("counterAttack", "Sprites/counterAttack", 3, 1, 1, Card.AbilityType.COUNTER_ATTACK));

        CardManagerAll.allcards.Add(new SpellCard("HEAL_ALL_FIELD_CARDS", "Sprites/heal_all_field_cards",  2, SpellCard.SpellType.HEAL_ALL_FIELD_CARDS, 2,
            SpellCard.TargetType.NO_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("DAMAGE_ENEMY_FIELD_CARDS", "Sprites/damage_enemy_field_cards", 2, SpellCard.SpellType.DAMAGE_ENEMY_FIELD_CARDS , 2,
            SpellCard.TargetType.NO_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("HEAL_ALL_HERO", "Sprites/heal_all_hero",  2, SpellCard.SpellType.HEAL_ALL_HERO , 2,
            SpellCard.TargetType.NO_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("DAMAGE_ENEMY_HERO", "Sprites/damage_enemy_hero", 2, SpellCard.SpellType.DAMAGE_ENEMY_HERO , 2,
            SpellCard.TargetType.NO_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("HEAL_ALL_CARD", "Sprites/heal_all_cards", 2, SpellCard.SpellType.HEAL_ALL_CARD , 2,
            SpellCard.TargetType.ALL_CARD_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("DAMAGE_ENEMY_CARD", "Sprites/damage_enemy_card",2, SpellCard.SpellType.DAMAGE_ENEMY_CARD , 2,
            SpellCard.TargetType.ENEMY_CARD_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("SHIELD_ON_ALL_CARD", "Sprites/shield_on_all_cards",2, SpellCard.SpellType.SHIELD_ON_ALL_CARD , 2,
            SpellCard.TargetType.ALL_CARD_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("PROVOCATION_ON_ALL_CARD", "Sprites/provocation_on_all_cards",2, SpellCard.SpellType.PROVOCATION_ON_ALL_CARD , 2,
            SpellCard.TargetType.ALL_CARD_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("BUFF_CARD_DAMAGE", "Sprites/buff_card_damage",2, SpellCard.SpellType.BUFF_CARD_DAMAGE , 2,
            SpellCard.TargetType.ALL_CARD_TARGET));
        CardManagerAll.allcards.Add(new SpellCard("DEBUFF_CARD_DAMAGE", "Sprites/debuff_card_damage", 2, SpellCard.SpellType.DEBUFF_CARD_DAMAGE , 2,
            SpellCard.TargetType.ENEMY_CARD_TARGET));

    }

    

}
