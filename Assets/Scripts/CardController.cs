﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour
{
    public Card Card;

    public bool IsPlayerCard;

    public CardInfoScript Info;
    public CardmovementScript1 Movement;
    public CardAbilities Ability;

    GameManagerScript gameManeger;

    public void Init(Card card, bool isPlayerCard)
    {
        Card = card;
        gameManeger = GameManagerScript.Instance;
        IsPlayerCard = isPlayerCard;

        if (isPlayerCard)
        {
            Info.ShowCardInfo();
            GetComponent<AttackCard>().enabled = false;

        }
        else
        {
            Info.HideCardInfo();
        }

        
    }
    public void OnCast()
    {

        if (Card.IsSpell && ((SpellCard)Card).SpellTarget != SpellCard.TargetType.NO_TARGET)
            return;

        
        if(IsPlayerCard)
        {
            gameManeger.PlayerHandCards.Remove(this);
            gameManeger.PlayerFieldCards.Add(this);
            gameManeger.ReduceMana(true, Card.ManaCost);
            gameManeger.CheckCardsForManaAvaliability();
        }
        else
        {
            
            gameManeger.EnemyHandCards.Remove(this);
            gameManeger.EnemyFieldCards.Add(this);
            gameManeger.ReduceMana(false, Card.ManaCost);
            Info.ShowCardInfo();
        }

        Card.IsPlaced = true;

        if (Card.HasAbility)
            Ability.OnCast();

        if (Card.IsSpell)
            UseSpell(null);

        UIController.Instance.UpdateHpAndMana();
    }

    public void OnTakeDamage(CardController attaker = null)
    {
        CheckForAlive();
        Ability.OnTookDamage(attaker);
    }

    public void OnDamageDeal()
    {
        Card.TimesDealDamage++;
        Card.CanAttack = false;
        Info.HighlightedCard(false);

        if (Card.HasAbility)
            Ability.OnDamageDeal();
    }


    public void UseSpell(CardController target)
    {
        var spellCard = ((SpellCard)Card);
        switch (spellCard.Spell)
        {
            case SpellCard.SpellType.HEAL_ALL_FIELD_CARDS:
                var allyCards = IsPlayerCard ?
                                gameManeger.PlayerFieldCards :
                                gameManeger.EnemyFieldCards;

                foreach(var card in allyCards)
                {
                    card.Card.Defence += spellCard.SpellValue;
                    card.Info.RefreshData();
                }

                break;
            case SpellCard.SpellType.DAMAGE_ENEMY_FIELD_CARDS:

                var enemyCards = IsPlayerCard ?
                                 new List<CardController>(gameManeger.EnemyFieldCards) :
                                 new List<CardController>(gameManeger.PlayerFieldCards);

                foreach (var card in enemyCards)
                    GiveDamageTo(card, spellCard.SpellValue);
                

                break;

            case SpellCard.SpellType.HEAL_ALL_HERO:

                if (IsPlayerCard)
                    gameManeger.CurrentGame.Player.HP += spellCard.SpellValue;
                else
                    gameManeger.CurrentGame.Enemy.HP += spellCard.SpellValue;

                UIController.Instance.UpdateHpAndMana();
                break;

            case SpellCard.SpellType.DAMAGE_ENEMY_HERO:

                if (IsPlayerCard)
                    gameManeger.CurrentGame.Enemy.HP -= spellCard.SpellValue;
                else
                    gameManeger.CurrentGame.Player.HP -= spellCard.SpellValue;

                UIController.Instance.UpdateHpAndMana();
                gameManeger.CheckForResult();


                break;

            case SpellCard.SpellType.HEAL_ALL_CARD:
                target.Card.Defence += spellCard.SpellValue;
                break;

            case SpellCard.SpellType.DAMAGE_ENEMY_CARD:
                GiveDamageTo(target, spellCard.SpellValue);
                break;

            case SpellCard.SpellType.SHIELD_ON_ALL_CARD:
                if (!target.Card.Abilities.Exists(x => x == Card.AbilityType.SHIELD))
                    target.Card.Abilities.Add(Card.AbilityType.SHIELD);
                break;

            case SpellCard.SpellType.PROVOCATION_ON_ALL_CARD:
                if (!target.Card.Abilities.Exists(x => x == Card.AbilityType.PROVOCATION))
                    target.Card.Abilities.Add(Card.AbilityType.PROVOCATION);
                break;

            case SpellCard.SpellType.BUFF_CARD_DAMAGE:
                target.Card.Attack += spellCard.SpellValue;
                break;

            case SpellCard.SpellType.DEBUFF_CARD_DAMAGE:
                target.Card.Attack = Mathf.Clamp(target.Card.Attack - spellCard.SpellValue, 0, int.MaxValue);
                break;

        }
        if(target != null)
        {
            target.Ability.OnCast();
            target.CheckForAlive();
        }

        DestroyCard();

    }

    void GiveDamageTo(CardController card, int damage)
    {
        card.Card.GetDamage(damage);
        card.CheckForAlive();
        card.OnTakeDamage();
    }
    public void CheckForAlive()
    {
        ;
        if (Card.IsAlive)
            Info.RefreshData();
        else
        {
            
            DestroyCard();
        }
            
    }

    public void DestroyCard()
    {
        Movement.OnEndDrag(null);

        RemoveCardFromList(gameManeger.EnemyFieldCards);
        RemoveCardFromList(gameManeger.EnemyHandCards);
        RemoveCardFromList(gameManeger.PlayerFieldCards);
        RemoveCardFromList(gameManeger.PlayerHandCards);

        Destroy(gameObject);
    }

    void RemoveCardFromList(List<CardController> list)
    {
        if (list.Exists(x => x == this))
            list.Remove(this);
    }
}
