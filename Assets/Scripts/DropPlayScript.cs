﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public enum FieldType
{
    SELF_HAND,
    SELF_FIELD,
    ENEMY_FIELD,
    ENEMY_HAND
}
public class DropPlayScript : MonoBehaviour,IDropHandler,IPointerEnterHandler,IPointerExitHandler 
{

    public FieldType Type;
    
    public void OnDrop(PointerEventData eventData)
    {

        if (Type != FieldType.SELF_FIELD)
            return;

        CardController card = eventData.pointerDrag.GetComponent<CardController>();

        if(card  
            && GameManagerScript.Instance.IsPlayerTurn 
            && GameManagerScript.Instance.CurrentGame.Player.Mana >= card.Card.ManaCost
            && !card.Card.IsPlaced)
        {
            if(!card.Card.IsSpell)
                card.Movement.DefaultParant = transform;

            card.OnCast();

        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null || Type == FieldType.ENEMY_FIELD || Type == FieldType.ENEMY_HAND || Type == FieldType.SELF_HAND) // Проверка на наличие объекта в руке
            return;

        CardmovementScript1 card = eventData.pointerDrag.GetComponent<CardmovementScript1>(); // Получаем объект

        if (card)
            card.DefaultTempCardParent = transform;

        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null )
            return;

        CardmovementScript1 card = eventData.pointerDrag.GetComponent<CardmovementScript1>();

        if (card && card.DefaultTempCardParent == transform) // возвращает карту в руку
            card.DefaultTempCardParent = card.DefaultParant;
    }
}
