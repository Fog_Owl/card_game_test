﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CardInfoScript : MonoBehaviour
{
    public CardController CC;

    public GameObject ObjToHighLighted;


    public Image Logo;
    public Text Name, Attack, Defence, Manacost;
    public GameObject HideObj, HighLightedObj;
    
    public Color NormalCol, TargetCol, SpellTargetCol;


    public void HideCardInfo()
    {
        
        HideObj.SetActive(true);
        Manacost.text = "";
        
    }
    public void ShowCardInfo()
    {


        Debug.Log("show");
        Logo.sprite = CC.Card.Logo;
        Logo.preserveAspect = true; // Сохраняет ли исходное соотношение сторон
        Name.text = CC.Card.Name;

        if(CC.Card.IsSpell)
        {
            Attack.gameObject.SetActive(false);
            Defence.gameObject.SetActive(false);
        }
        HideObj.SetActive(false);

        RefreshData();

    }

    public void RefreshData()
    {
        Attack.text = CC.Card.Attack.ToString();
        Defence.text = CC.Card.Defence.ToString();
        Manacost.text = CC.Card.ManaCost.ToString();
    }


    public void HighlightedCard(bool highlight)
    {
        HighLightedObj.SetActive(highlight);
    }
  

    public void HighLightAvaliAbility(int currentMana)
    {
        GetComponent<CanvasGroup>().alpha = currentMana >= CC.Card.ManaCost ? 1 : .5f;
    }

    public void HighlightAsTarget(bool highlight)
    {
        
        ObjToHighLighted.GetComponent<Image>().color = highlight ?
                                      TargetCol :
                                      NormalCol;
    }

    public void HighlightAsSpellTarget(bool highlight)
    {

        ObjToHighLighted.GetComponent<Image>().color = highlight ?
                                      SpellTargetCol :
                                      NormalCol;
    }

}
