﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{
    public static UIController Instance;

    public Text PlayerMana, EnemyMana;
    public Text PlayerHp, EnemyHp;

    public GameObject ResultGo;
    public Text ResultTxt;

    public Text TurnTime;
    public Button EndTurnBtn;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }

    public void StartGame()
    {
        EndTurnBtn.interactable = true;
        ResultGo.SetActive(false);
        UpdateHpAndMana();
    }

    public void UpdateHpAndMana()
    {
        PlayerMana.text = GameManagerScript.Instance.CurrentGame.Player.Mana.ToString();
        EnemyMana.text = GameManagerScript.Instance.CurrentGame.Enemy.Mana.ToString();
        EnemyHp.text = GameManagerScript.Instance.CurrentGame.Enemy.HP.ToString();
        PlayerHp.text = GameManagerScript.Instance.CurrentGame.Player.HP.ToString();
    }

    public void ShowResults()
    {
        ResultGo.SetActive(true);

        if (GameManagerScript.Instance.CurrentGame.Enemy.HP == 0)
            ResultTxt.text = "Win";
        else
            ResultTxt.text = "-25";
    }

    public void UpdateTurnTime(int time)
    {
        TurnTime.text = time.ToString();
    }

    public void DisableTurnBtn()
    {
        EndTurnBtn.interactable = GameManagerScript.Instance.IsPlayerTurn;
    }
}
